var $container = document.getElementById('excel');
var hot = new Handsontable($container, {
rowHeaders: true,
contextMenu: true,
filters: true,
dropdownMenu: true,
licenseKey:"non-commercial-and-evaluation"
});

var clazz = $("#class");
var methodz = $("#methods");
var save = $("#retain");

function mainList(){

var option = clazz.children('option:selected').val();

$.ajax({
type: "POST",
contentType: "application/json",
url: "/getMethods",
data: JSON.stringify({methodName:option}),
dataType: 'json',
cache: false,
timeout: 600000,
success: function (response) {
console.log(response);

$('#methods option').remove();

$.each(response, function(index,methodName) {
methodz.append('<option value="'+methodName+'">'+methodName+'</option>')
});

},
error:function (response) {
console.log(response);
}
})

}

function loadData(){

var clazzName = clazz.children('option:selected').val();
var methodzName = methodz.children('option:selected').val();

$.ajax({
type: "POST",
contentType: "application/json",
url: "/loadData",
data: JSON.stringify({className:clazzName,methodName:methodzName}),
cache: false,
timeout: 600000,
success: function (response) {

hot.loadData(response.tableData);
hot.updateSettings({
colHeaders:response.tableMetadata,
columns:response.tableMetadataDisabled
});

$('#retain button').remove();
var $button = $('<button id="save" onclick="postData()">Save</button>');
$button.appendTo(save);

},
error:function (response) {
console.log(response);
}
})
}

function postData(){

var clazzName = clazz.children('option:selected').val();
var methodzName = methodz.children('option:selected').val();
var tableName = clazzName+"_"+methodzName;

$.ajax({
type: "POST",
contentType: "application/json",
url: "/saveTableData",
data: JSON.stringify({data:tableName, dataTable: hot.getSourceData()}),
dataType: 'json',
cache: false,
timeout: 600000,
success: function (response) {
console.log(response);
},
error:function (response) {
console.log(response);
}
})

}
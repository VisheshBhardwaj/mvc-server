package com.server.data.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class Utils {

    public String getRandomString(int length) {
        StringBuilder buildString = new StringBuilder();
        String characterSet = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

        for (int i = 0; i < length; i++) {
            int index = (int) (Math.random() * characterSet.length());
            buildString.append(characterSet.charAt(index));
        }
        return buildString.toString();
    }

    public String convertToString(Object object){
        try {
            ObjectMapper objectWriter = new ObjectMapper();
            return objectWriter.writeValueAsString(object);
        } catch (JsonProcessingException e) {
            return null;
        }
    }

}

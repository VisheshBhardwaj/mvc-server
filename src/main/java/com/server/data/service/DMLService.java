package com.server.data.service;

import com.server.data.model.Classdata;
import com.server.data.utils.Utils;
import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.sql.DataSource;
import java.util.*;

import static java.util.Collections.*;

@Service
@AllArgsConstructor
public class DMLService {

    private DataSource dataSource;


    public int createTable(String className,String methodName){
        try {
            JdbcTemplate jdbc = new JdbcTemplate(dataSource);
            jdbc.execute("CREATE TABLE ClassData_" + className + "_" + methodName + " (ID TEXT PRIMARY KEY,ExecutionStatus TEXT, TestCaseId TEXT);");
            jdbc.execute("INSERT INTO ClassData_" + className + "_" + methodName + "(ID , ExecutionStatus) VALUES ('"+new Utils().getRandomString(8) +"', 'Y')");
            return 0;
        }catch (Exception e){
            return 1;
        }
    }

    public List<String> getTables(){
        try {
            JdbcTemplate jdbc = new JdbcTemplate(dataSource);
            List<String> list = new ArrayList<>();

            for(Map<String,Object> row : jdbc.queryForList("SELECT name FROM sqlite_master WHERE type='table';")){
                if(row.get("name").toString().contains("ClassData_")){
                    list.add(row.get("name").toString().replace("ClassData_",""));
                }
            }
            return list;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public Map<String,List<String>> getTablesName(){
        try {
            JdbcTemplate jdbc = new JdbcTemplate(dataSource);
            Map<String,List<String>> map = new HashMap<>();

            for(Map<String,Object> row : jdbc.queryForList("SELECT name FROM sqlite_master WHERE type='table';")){
                if(row.get("name").toString().contains("ClassData_")){
                    String completeName = row.get("name").toString().replace("ClassData_","");
                    String split[] = completeName.split("_");

                    if(!map.keySet().contains(split[0])){
                        map.put(split[0],new ArrayList<>());
                        map.get(split[0]).add(split[1]);
                    }else{
                        map.get(split[0]).add(split[1]);
                    }
                }
            }
            return map;
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }
    }

    public List<String> getTableMetadataAsList(Classdata classdata){
        JdbcTemplate jdbc = new JdbcTemplate(dataSource);
        List<Map<String,Object>> metaMap = jdbc.queryForList("pragma table_info(ClassData_"+classdata.getData()+");");
        List<String> list = new ArrayList<>();
        for(Map<String,Object> row :metaMap){
            list.add(row.get("name").toString());
        }
        return list;
    }

    public String getTableMetadataAsString(Classdata classdata){
        StringBuilder metadata = new StringBuilder();
        for(String data: this.getTableMetadataAsList(classdata)){
            metadata.append(data.strip()+",");
        }
        return metadata.toString().substring(0,metadata.toString().lastIndexOf(","));
    }

    public String getTableMetadataAsSortedString(Classdata classdata){
        StringBuilder metadata = new StringBuilder();
        List<String> list = this.getTableMetadataAsList(classdata);
        sort(list);
        for(String data: list){
            metadata.append(data.strip()+",");
        }
        return metadata.toString().substring(0,metadata.toString().lastIndexOf(","));
    }
    /**
     * Possibly the most destructive part
     * @param classdata
     * @return
     */
    public boolean setTableMetadata(Classdata classdata){
        JdbcTemplate jdbc = new JdbcTemplate(dataSource);
        List<String> oldMetaData = new LinkedList<String>(getTableMetadataAsList(classdata));
        List<String> newMetaData = Arrays.asList(classdata.getMetaData().split(","));

        boolean flag = true;

        for(int index=0;index<newMetaData.size();index++){// Check if New Fields were introduced

            String metaData = newMetaData.get(index);

            if(!oldMetaData.contains(metaData)){//New Coloum
                try{
                    jdbc.execute("ALTER TABLE ClassData_"+classdata.getData()+" ADD "+metaData+" TEXT;");
                }catch (Exception e){
                    System.out.println("Issue while Altering table");
                    flag = false;
                }
            }else{
                oldMetaData.remove(oldMetaData.indexOf(metaData));
            }
        }
        /**
         * oldMetaData now has fields which need to be removed
         */
        if(oldMetaData.size()>0) {

            /**
             * SQLite does't have feature to drop coloums so
             * 1) Create table copy & copy "necessory" data
             * 2) Drop old table
             * 3) Rename copied table to new table
             */

            List<String> currentMetaData = new LinkedList<>(getTableMetadataAsList(classdata));

            for (String metaData : oldMetaData) {// Check if Old Fields were removed
                currentMetaData.remove(metaData);
            }

            // 1) Create table copy & copy "necessory-" data using "Create Table AS"
            jdbc.execute("CREATE TABLE Temp_" + classdata.getData() + " AS SELECT " + String.join(", ", currentMetaData) + " FROM ClassData_" + classdata.getData()+ ";");
            //2) Drop old table
            jdbc.execute("DROP TABLE ClassData_"+ classdata.getData() + ";");
            //3) Rename copied table to new table
            jdbc.execute("ALTER TABLE Temp_"+ classdata.getData() + " RENAME TO ClassData_"+ classdata.getData() + ";");
        }

        return flag;
    }

}


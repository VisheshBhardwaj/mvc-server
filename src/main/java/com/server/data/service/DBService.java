package com.server.data.service;

import lombok.Data;
import org.springframework.jdbc.core.JdbcTemplate;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Data
public class DBService {
    private String table;
    private DataSource dataSource;
    private JdbcTemplate template;

    public DBService(DataSource dataSource){
        this.dataSource = dataSource;
        template = new JdbcTemplate(dataSource);
    }

    //<S extends T> S save(S entity);

    public List<Map<String,Object>> findAll(){
        try {
            return template.queryForList("SELECT * FROM "+ table +";");
        }catch (Exception e){
            return null;
        }
    }

    public int count(){
        return this.findAll().size();
    }

    public boolean deleteById(String id){
        try{
            int rows = template.update("DELETE FROM "+table+" WHERE ID = '"+id+"';");
            if(rows==1){
                return true;
            }else{
                return false;
            }
        }catch (Exception e){
            return false;
        }
    }

    public boolean save(Map<String,Object> row){
        try{
            List<String> list = new ArrayList<>();

            for(Object data : row.values()) {
                list.add(String.valueOf(data));
            }

            String Values = "\""+String.join("\", \"",list)+"\"";

            int rows = template.update("INSERT OR REPLACE INTO "+table+"("+String.join(", ",row.keySet())+") VALUES("+Values+");");
            if(rows==1){
                return true;
            }else{
                return false;
            }
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }
}

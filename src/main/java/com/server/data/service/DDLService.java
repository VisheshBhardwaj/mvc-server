package com.server.data.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.server.data.model.Classdata;
import com.server.data.model.Metadata;
import com.server.data.utils.Utils;
import lombok.AllArgsConstructor;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Service
@AllArgsConstructor
public class DDLService {

    private DataSource dataSource;

    public List<Map<String, Object>> getTableData(String... data) {
        try {
            JdbcTemplate template = new JdbcTemplate(dataSource);
            if(data.length==2){
                return template.queryForList("SELECT * FROM ClassData_" + data[0] + "_" + data[1] + ";");
            }
            if(data.length==1){
                return template.queryForList("SELECT * FROM ClassData_" + data[0] + ";");
            }
        }catch (Exception e){
            return null;
        }
        return null;
    }

    public String getTableDataAsJSONString(String className,String methodName) {
        try {
            ObjectMapper objectWriter = new ObjectMapper();
            return objectWriter.writeValueAsString(this.getTableData(className,methodName));
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    public String getTableDataAsJSONString(String classData) {
        try {
            ObjectMapper objectWriter = new ObjectMapper();
            return objectWriter.writeValueAsString(this.getTableData(classData));
        } catch (JsonProcessingException e) {
            return null;
        }
    }

    public List<Metadata> getTableMetadataAsListWithDisabledData(Classdata classdata){
        JdbcTemplate jdbc = new JdbcTemplate(dataSource);
        List<Map<String,Object>> metaMap = jdbc.queryForList("pragma table_info(ClassData_"+classdata.getData()+");");
        List<Metadata> list = new ArrayList<>();
        for(Map<String,Object> row :metaMap){
            Metadata meta = new Metadata();

            if(row.get("name").equals("ID")) {
                meta.setData("ID");
                meta.setEditor(false);
                list.add(meta);
            }else {
                meta.setData(row.get("name").toString());
                meta.setEditor("text");
                list.add(meta);
            }
        }
        return list;
    }

    /**
     * Add New Data & Remove Old Data
     * LOGIC
     * First Iterate On New Data
     * if ID (NOT) PRESENT -> Create ID & Insert
     * else INSERT OR REPLACE Data
     * Now New Data Won't have old data if Data was Removed So
     * Iterate over old Data Or All Data & use individual ID of OLD Or All Data to check if its present in new Data
     * using All Data of ID -> ID is not Found in New Data, Delete Record END
     */
    public boolean setTableData(String table,List<Map<String,Object>> newData) {
        DBService dbService = new DBService(dataSource);
        dbService.setTable("ClassData_"+table);
        List<Boolean> log = new ArrayList<>();

        for(Map<String,Object> row : newData) {
            if(ObjectUtils.isEmpty(row.get("ID"))){
                row.put("ID", new Utils().getRandomString(8));
                log.add(dbService.save(row));
            }else {
                log.add(dbService.save(row));
            }
        }
        List<Map<String,Object>> allData = this.getTableData(table);

        for(Map<String,Object> orow : allData) {// Iterate Over All Data
            if(newData.stream().anyMatch(nrow -> nrow.get("ID").equals(orow.get("ID")))) {//If Stream is True Then Do nothing(Means Id Was Found in New Data)

            }else {//Stream False if Empty (Record Not Found in New data) So Delete Record
                log.add(dbService.deleteById(orow.get("ID").toString()));
            }
        }

        if(log.contains(false)){
            return false;
        }else{
            return true;
        }
    }


}

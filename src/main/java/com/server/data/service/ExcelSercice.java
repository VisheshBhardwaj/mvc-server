package com.server.data.service;

import com.server.data.model.Excel;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileInputStream;
import java.util.*;

public class ExcelSercice {

    private FileInputStream excelReader;

    public Excel fileUpload(MultipartFile file){
        Workbook workbook = readWorkBook(file,excelReader);
        Map<String,List<Map<String,String>>> excelData = new HashMap<>();

        if(workbook!=null){
            Iterator<Sheet> it = workbook.sheetIterator();
            List<String> sheets = new ArrayList<>();
            while(it.hasNext()) {
                Sheet sheet = it.next();
                sheets.add(sheet.getSheetName());
                String[][] data = new String[sheet.getLastRowNum()][sheet.getRow(0).getLastCellNum()];

                for(int row = 0;row<sheet.getLastRowNum();row++){
                    Row rowData = sheet.getRow(row);
                    Iterator<Cell> ct = rowData.cellIterator();
                    int col = 0;
                    while(ct.hasNext()){
                        Cell cell = ct.next();
                        data[row][col] = cell.getStringCellValue();
                        col++;
                    }
                }
                excelData.put(sheet.getSheetName(),tomap(data));
            }

            try {
                workbook.close();
                excelReader.close();
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

            return new Excel(file.getOriginalFilename(),sheets,excelData);
        }else{
            return null;
        }
    }

    public Workbook readWorkBook(MultipartFile file,FileInputStream excelReader){
        File currDir = new File(".");
        String path = currDir.getAbsolutePath();
        String fileLocation = path.substring(0, path.length() - 1) + file.getOriginalFilename();
        try {
            excelReader = new FileInputStream(fileLocation);
            Workbook workbook = new XSSFWorkbook(excelReader);
            return workbook;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Map<String,String>> tomap(String[][] data){
        List<Map<String,String>> list = new ArrayList<>();
        for(int row = 0; row<data.length;row++){
            HashMap<String,String> map = new HashMap<>();

            for(int mrow = 0;mrow<data[0].length;mrow++){
                map.put(data[0][mrow],data[row][mrow]);
            }

            list.add(map);
        }
        return list;
    }

}

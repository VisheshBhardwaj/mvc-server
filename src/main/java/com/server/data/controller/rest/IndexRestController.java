package com.server.data.controller.rest;

import com.server.data.model.Classdata;
import com.server.data.service.DDLService;
import com.server.data.service.DMLService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

@RestController
public class IndexRestController extends BaseRestController{

    @PostMapping("/getMethods")
    public ResponseEntity<?> getClassTables(@RequestBody Classdata classdata){

        DMLService dmlService = new DMLService(dataSource);
        return new ResponseEntity<>(dmlService.getTablesName().get(classdata.getMethodName()), HttpStatus.OK);

    }

    @PostMapping("/loadData")
    public ResponseEntity<?> loadData(@RequestBody Classdata classdata) {

        HashMap<String, Object> model = new HashMap<>();

        DDLService ddlService = new DDLService(dataSource);
        DMLService dmlService = new DMLService(dataSource);
        model.put("tableData", ddlService.getTableData(classdata.getClassName(), classdata.getMethodName()));
        classdata.setData(classdata.getClassName() + "_" + classdata.getMethodName());
        model.put("tableMetadataDisabled", ddlService.getTableMetadataAsListWithDisabledData(classdata));
        model.put("tableMetadata", dmlService.getTableMetadataAsList(classdata));

        return new ResponseEntity<>(model, HttpStatus.OK);
    }

}

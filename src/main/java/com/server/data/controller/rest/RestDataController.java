package com.server.data.controller.rest;

import com.server.data.model.Classdata;
import com.server.data.service.DDLService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class RestDataController extends BaseRestController{

    @GetMapping(value = "/rest/{className}/{method}")
    public @ResponseBody ResponseEntity getTableData(@PathVariable String className,@PathVariable String method) {
        JdbcTemplate template = new JdbcTemplate(dataSource);
        try{
            List<Map<String, Object>> data =  template.queryForList("SELECT * FROM ClassData_"+className+"_"+method+";");
            return new ResponseEntity<>(data, HttpStatus.OK);
        }catch (Exception e){
            return new ResponseEntity<>(e, HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/saveTableData")
    public ResponseEntity<?> saveTableData(@RequestBody Classdata classdata){
        DDLService ddlService = new DDLService(dataSource);
        if(ddlService.setTableData(classdata.getData(),classdata.getDataTable())){
            return (new ResponseEntity<>(HttpStatus.CREATED));
        }else{
            return (new ResponseEntity<>(HttpStatus.CONFLICT));
        }

    }
}

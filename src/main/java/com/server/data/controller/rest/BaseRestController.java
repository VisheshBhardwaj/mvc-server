package com.server.data.controller.rest;

import com.server.data.service.LogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RestController;

import javax.sql.DataSource;

@RestController
public class BaseRestController {
    @Autowired
    @Qualifier("commonDataSource")
    protected DataSource dataSource;

    @Autowired
    protected LogService logger;

}

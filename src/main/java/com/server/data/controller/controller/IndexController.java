package com.server.data.controller.controller;

import com.server.data.model.Classdata;
import com.server.data.service.DDLService;
import com.server.data.service.DMLService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class IndexController extends BaseController{

    @GetMapping(value = {"/index","/"})
    public String homePage(ModelMap model) {
        DMLService dmlService = new DMLService(dataSource);
        model.put("tables",dmlService.getTablesName().keySet());
        model.addAttribute("classdata",new Classdata());
        return "index";
    }

    @PostMapping("/getTableData")
    public String getTableData(@ModelAttribute Classdata classdata, BindingResult result, ModelMap model){
        if(result.hasErrors()){
            return "error";
        }
        DMLService dmlService = new DMLService(dataSource);
        model.put("tables",dmlService.getTables());
        DDLService ddlService = new DDLService(dataSource);
        model.put("tableName",classdata.getData());
        model.put("tableData",ddlService.getTableDataAsJSONString(classdata.getData()));
        model.put("tableMetadata",dmlService.getTableMetadataAsList(classdata));
        model.put("tableMetadataDisabled",ddlService.getTableMetadataAsListWithDisabledData(classdata));
        return "index";
    }

}

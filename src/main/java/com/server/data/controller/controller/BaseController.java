package com.server.data.controller.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;

import javax.sql.DataSource;

@Controller
public class BaseController {

    @Autowired
    @Qualifier("commonDataSource")
    protected DataSource dataSource;

}

package com.server.data.controller.controller;

import com.server.data.model.Classdata;
import com.server.data.service.DMLService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class DMLController extends BaseController{

    @PostMapping(value = "/createTable")
    public String createTable(@ModelAttribute Classdata classData, BindingResult result, ModelMap model){
        if(result.hasErrors()){
            return "error";
        }
        DMLService dmlService = new DMLService(dataSource);
        model.put("successTableCreate",dmlService.createTable(classData.getClassName(),classData.getMethodName()));
        return "createTable";
    }

    @GetMapping(value = "/createTable")
    public String getTable(@ModelAttribute Classdata classData, BindingResult result, ModelMap model){
        if(result.hasErrors()){
            return "error";
        }
        DMLService dmlService = new DMLService(dataSource);
        model.put("successTableCreate",dmlService.createTable(classData.getClassName(),classData.getMethodName()));
        return "createTable";
    }
}

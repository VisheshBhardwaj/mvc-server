package com.server.data.controller.controller;

import com.server.data.model.dao.user.User;
import com.server.data.repository.user.UserRepository;
import com.server.data.utils.Utils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class LoginController extends BaseController{

    @Autowired
    UserRepository userRepository;

    @Autowired
    PasswordEncoder passwordEncoder;

    @GetMapping(value = {"/login"})
    public String homePage(ModelMap model) {
        model.addAttribute("user",new User());
        return "login";
    }

    @PostMapping("/createUser")
    public String createUser(@ModelAttribute User user, BindingResult result, ModelMap model) {
        if (result.hasErrors()) {
            return "error";
        }
        try {
            user.setId(new Utils().getRandomString(8));
            user.setPassword(passwordEncoder.encode(user.getPassword()));
            user.setAccountNonExpired(false);
            user.setAccountNonLocked(false);
            user.setCredentialsNonExpired(false);
            user.setEnabled(false);
            userRepository.save(user);
        }catch (Exception e){
            e.printStackTrace();
        }
        return "login";
    }
}

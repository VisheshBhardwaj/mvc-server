package com.server.data.controller.controller;

import com.server.data.service.DMLService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TablesController extends BaseController{

    @GetMapping(value = {"/tables"})
    public String homePage(ModelMap model) {
        DMLService dmlService = new DMLService(dataSource);
        model.put("tables",dmlService.getTables());
        return "tables";
    }
}

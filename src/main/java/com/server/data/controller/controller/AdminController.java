package com.server.data.controller.controller;

import com.server.data.model.dao.user.User;
import com.server.data.repository.user.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.Arrays;

@Controller
public class AdminController extends BaseController{

    @Autowired
    UserRepository userRepository;

    @GetMapping("/admin")
    public String isAdminCheck(ModelMap model){
        return "index";
    }

    @GetMapping("/users")
    public String getUsers(ModelMap model){

        User user = new User();
        user.setAccountNonExpired(false);
        user.setAccountNonLocked(false);
        user.setCredentialsNonExpired(false);
        user.setEnabled(false);
        Example<User> query = Example.of(user);
        model.put("falseUsers",userRepository.findAll(query));
        model.put("user",new User());
        return "users";
    }

    @PostMapping("/user/{userId}")
    public String setUser(@PathVariable String userId, @ModelAttribute User user, BindingResult result, ModelMap model) {
        if(result.hasErrors()){
            System.out.println(Arrays.asList(result.getAllErrors()));
            return "error";
        }
        User currentUser = userRepository.getOne(userId);
        currentUser.setRole(user.getRole());
        currentUser.setCredentialsNonExpired(true);
        currentUser.setAccountNonLocked(true);
        currentUser.setAccountNonExpired(true);
        currentUser.setEnabled(true);
        userRepository.save(currentUser);
        return "redirect:/users";
    }
}

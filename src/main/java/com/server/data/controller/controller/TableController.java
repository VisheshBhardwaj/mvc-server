package com.server.data.controller.controller;

import com.server.data.model.Classdata;
import com.server.data.service.DMLService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class TableController extends BaseController{

    @GetMapping("/table/{table}")
    public String homePage(@PathVariable String table, ModelMap model) {
        DMLService dmlService = new DMLService(dataSource);
        Classdata classdata = new Classdata();
        classdata.setData(table);
        model.put("metaData",dmlService.getTableMetadataAsString(classdata));
        model.addAttribute("classdata",new Classdata());
        return "table";
    }

    @PostMapping("/table/{table}")
    public String alterTable(@PathVariable String table, @ModelAttribute Classdata classdata, BindingResult result, ModelMap model) {
        if(result.hasErrors()){
            return "error";
        }
        DMLService dmlService = new DMLService(dataSource);
        classdata.setData(table);
        dmlService.setTableMetadata(classdata);

        model.addAttribute("classdata",new Classdata());
        model.put("tables",dmlService.getTables());
        return "tables";
    }
}

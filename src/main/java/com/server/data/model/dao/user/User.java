package com.server.data.model.dao.user;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Table(name = "User")
@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class User {

    @Id
    private String id;
    private String password;
    private String username;
    private boolean credentialsNonExpired;
    private boolean enabled;
    private boolean accountNonExpired;
    private boolean accountNonLocked;
    private String email;
    private String role;
    //@OneToMany(fetch = FetchType.EAGER,cascade = CascadeType.ALL)
    //Role role;
}
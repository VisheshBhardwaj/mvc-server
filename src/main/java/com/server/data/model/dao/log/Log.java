package com.server.data.model.dao.log;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Log")
@Data
public class Log {

    @Id
    String id;
    String timestamp;
    String tableName;
}

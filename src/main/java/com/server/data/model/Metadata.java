package com.server.data.model;

import lombok.Data;

@Data
public class Metadata {
    private String data;
    private Object editor;
}
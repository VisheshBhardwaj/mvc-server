package com.server.data.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Map;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Classdata {
    private String className;
    private String methodName;
    private String data;
    private String metaData;
    private List<Map<String,Object>> dataTable;

}

package com.server.data.model;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;
import java.util.Map;

@Data
@AllArgsConstructor
public class Excel {
    String fileName;
    List<String> sheets;

    /**
     * Signifies Map of Sheets which has List of data
     */
    Map<String,List<Map<String,String>>> excelData;
}

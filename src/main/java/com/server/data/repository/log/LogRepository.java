package com.server.data.repository.log;

import com.server.data.model.dao.log.Log;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LogRepository extends JpaRepository<Log, String> {

}

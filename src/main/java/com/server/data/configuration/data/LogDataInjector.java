package com.server.data.configuration.data;

import com.server.data.repository.log.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableJpaRepositories(basePackageClasses = LogRepository.class, entityManagerFactoryRef = "logEntityManagerFactory",transactionManagerRef = "logTransactionManager")
@PropertySource("application-${spring.profiles.active}.properties")
public class LogDataInjector {

    @Autowired
    private Environment env;

    @Bean
    public DataSource logDataSource() {
        final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(env.getProperty("spring.datasource.driver-class-name"));
        dataSource.setUrl(env.getProperty("spring.second-datasource.url"));
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean logEntityManagerFactory() {
        final LocalContainerEntityManagerFactoryBean em = new LocalContainerEntityManagerFactoryBean();
        em.setDataSource(logDataSource());
        em.setPackagesToScan("com.server.data.model.dao.log");
        em.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        em.setJpaProperties(additionalProperties());
        return em;
    }

    final Properties additionalProperties() {
        final Properties hibernateProperties = new Properties();
        if (env.getProperty("spring.jpa.properties.hibernate.hbm2ddl.auto") != null) {
            hibernateProperties.setProperty("spring.jpa.properties.hibernate.hbm2ddl.auto", env.getProperty("spring.jpa.properties.hibernate.hbm2ddl.auto"));
            hibernateProperties.setProperty("hibernate.hbm2ddl.auto", env.getProperty("spring.jpa.properties.hibernate.hbm2ddl.auto"));
        }
        if (env.getProperty("spring.jpa.properties.hibernate.dialect") != null) {
            hibernateProperties.setProperty("spring.jpa.properties.hibernate.dialect", env.getProperty("spring.jpa.properties.hibernate.dialect"));
            hibernateProperties.setProperty("hibernate.dialect", env.getProperty("spring.jpa.properties.hibernate.dialect"));
        }
        return hibernateProperties;
    }

    @Bean
    public PlatformTransactionManager logTransactionManager() {
        JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(logEntityManagerFactory().getObject());
        return transactionManager;
    }

    @Configuration
    @Profile(value = {"test","prod"})
    @PropertySource("application-${spring.profiles.active}.properties")
    class SqliteConfig {}
}
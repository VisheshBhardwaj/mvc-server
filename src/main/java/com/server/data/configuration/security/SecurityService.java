package com.server.data.configuration.security;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

import java.util.Collection;

@Service
public class SecurityService {

    public Authentication getAuthentication(){
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (!(authentication instanceof AnonymousAuthenticationToken)) {
            return authentication;
        }else{
            return null;
        }
    }
    public String getUserName(){
        return getAuthentication().getName();
    }

    public Collection<? extends GrantedAuthority> getAuthorities(){
        return getAuthentication().getAuthorities();
    }
}
